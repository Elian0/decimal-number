function decimalToBinary() {
    var decimalNumber = parseInt(document.getElementById("decimalInput").value);
    if(isNaN(decimalNumber) || decimalNumber < 0) {
        document.getElementById("binaryOutput").innerHTML = "Ju lutem jepni një numër pozitiv të plotë.";
        return;
    }
    var binaryNumber = decimalNumber.toString(2);
    document.getElementById("binaryOutput").innerHTML = "Numri binar i konvertuar: " + binaryNumber;
}